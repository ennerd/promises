# The\Promises

Provides access to the Promises implementation through the The namespace. 


## Usage

This is an async implementation of fread.

```php
function async_fread($handle, $length) {
	return new \The\Promise(function($yes, $no) use($handle, $length) {
		\The\Loop::onReadable($file, function() use($yes) {
			$yes(fread($handle, $length));
		});
	});
}

// Using it synchronously:

$data = async_fread($handle, $length)->wait();
// Code continues here. The script simply pauses until the data is available.

// Using it asynchronously in amphp and similar frameworks:

$data = yield async_fread($handle, $length);
// Code continues here, but other stuff can be happening while waiting because of yield

// Using it asynchronously in other frameworks:

async_fread($handle, $length)->then(function($data) {
	// Code continues here
});
```


