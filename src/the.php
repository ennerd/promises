<?php
namespace The;

/**
 * The Guzzle Promise implementation is good. This library adopts it. Compatible implementations
 * can override this by creating class aliases for \The\PromiseInterface::class and 
 * \The\Promise::class on their own.
 */
class_alias( \GuzzleHttp\Promise\PromiseInterface::class, PromiseInterface::class );
class_alias( \GuzzleHttp\Promise\Promise::class, Promise::class );
